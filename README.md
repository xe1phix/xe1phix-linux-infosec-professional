# Xe1phix-Linux-InfoSec-Professional

<p align="center">
  <a href="https://telegram.me/xe1phix">
    <img src="https://img.shields.io/badge/Telegram-%40Xe1phix-blue?style=flat&logo=telegram" alt="Telegram @Xe1phix">
  </a>
  <a href="https://secdsm.slack.com">
    <img src="https://img.shields.io/badge/Slack-%40Xe1phix-blueviolet?style=flat&logo=slack" alt="SecDSM Slack @Xe1phix">
  </a>
  <a href="https://discord.com/channels/1002774831961481247/1002817290875703296">
    <img src="https://img.shields.io/discord/1002774831961481247?color=informational&label=VX-Underground%20Discord&logo=discord&style=plastic" alt="VX-Underground Discord @Xe1phix">
  </a>
  <a href="https://discord.com/channels/1002774831961481247/1002817290875703296">
    <img src="https://img.shields.io/discord/399328329385246730?color=red&label=SecDSM%20Discord&logo=discord&style=plastic" alt="SecDSM Discord @Xe1phix">
  </a>
  <a href="https://matrix.to/#/#parrotsec:matrix.org">
    <img src="https://img.shields.io/matrix/parrotsec:matrix.org?label=ParrotSec%20Matrix&logo=matrix&logoColor=green&style=plastic" alt="ParrotSec Matrix Group @Xe1phix">
  </a>
  <a href="https://twitter.com/xe1phix">
    <img src="https://img.shields.io/twitter/url/https/xe1phix?label=%40Xe1phix&logo=twitter&style=flat" alt="Twitter @Xe1phix">
  </a>
  <a href="https://gitlab.com/xe1phix/Gnupg/blob/master/Xe1phix.asc">
    <img src="https://img.shields.io/badge/Xe1phix's-GnuPG%20Key-red?style=flat&logo=gnu" alt="Xe1phix's GnuPG Key">
  </a>
  <a href="mailto:xe1phix@protonmail.ch">
    <img src="https://img.shields.io/badge/Xe1phix-%40protonmail.ch-blue?style=plastic&logo=gnu" alt="ProtonMail - Xe1phix">
  </a>
  <a href="https://gitlab.com/xe1phix/Gnupg/blob/master/Xe1phix_protonmail.ch.asc">
    <img src="https://img.shields.io/badge/Xe1phix-%40protonmail.ch-blue?style=plastic&logo=gnu" alt="Xe1phix - ProtonMail Public Key">
  </a>
</p>


## Xe1phix - Conference Recordings:
- Portland Linux User Group (PLUG) - Secure Linux VPN Use v2
- [SecDSM - Securing Linux VPNs](https://www.youtube.com/watch?v=rtFKJR3Siz0)
- CornCon 7 - Secure Linux Networking v2
- [CornCon 6 - Secure Linux Networking](https://www.youtube.com/watch?v=zIUND1dZyhs)
- [CornCon 5 - Intro To Linux FileSystems v2](https://www.youtube.com/watch?v=zjjF4yebF_M)
- [SecDSM - Intro To Linux Filesystems](https://www.youtube.com/watch?v=8tNLKZAojL8)
- [SecDSM - How To Create A Persistent, LUKS Encrypted USB Device](https://www.youtube.com/watch?v=A1PJHn-tYrc)
- [CornCon 4 - Building A Restricted and Trustworthy Linux Environment](https://www.youtube.com/watch?v=W47f2cke4Vg&pbjreload=10)
- CentralCon - Encrypting Files With Friends Using GnuPG On Parrot Linux
- CornCon 3 - How To Create A Hackers Swiss Army Knife With Parrot Linux


## Xe1phix - Conference Talk Slides:
- [Portland Linux User Group (PLUG) - Secure Linux VPN Use v2](https://pdxlinux.org/2023-10-05-Secure-Linux-VPN-Use.pdf)
- [SecDSM - Securing Linux VPNs](https://gitlab.com/xe1phix/ParrotSecWiki/-/blob/InfoSecTalk/Xe1phix-InfoSec-Talk-Materials/Securing-Linux-VPNs-SecDSM-Nov-2021/Xe1phix-Securing-Linux-VPNs-_v4.57.87_.pdf)
- [CornCon 7 - Secure Linux Networking v2](https://gitlab.com/xe1phix/ParrotSecWiki/-/blob/InfoSecTalk/Xe1phix-InfoSec-Talk-Materials/Secure-Linux-Networking-v2-%5BCornCon-2021%5D/Secure-Linux-Networking-v2-%5BSlides%5D/Xe1phix-Secure-Linux-Networking-v2-_v12.5.54_.pdf)
- [CornCon 6 - Secure Linux Networking](https://gitlab.com/xe1phix/ParrotSecWiki/-/blob/InfoSecTalk/Xe1phix-InfoSec-Talk-Materials/Secure-Linux-Networking-%5BCornCon-2020%5D/Secure-Linux-Networking-%5BSlides%5D/Xe1phix-Secure-Linux-Networking-v2-_v10.4.8_.odp)
- [CornCon 5 - Intro To Linux FileSystems v2](https://gitlab.com/xe1phix/ParrotSecWiki/blob/InfoSecTalk/Xe1phix-InfoSec-Talk-Materials/IntroToLinuxFilesystems-CornCon-September2019/%20Xe1phix-Linux-FileSystems-Slides/IntroToLinuxFilesystems-CornCon-LibreOffice-v14.5.odp)
- [SecDSM - Intro To Linux Filesystems](https://gitlab.com/xe1phix/ParrotSecWiki/blob/InfoSecTalk/Xe1phix-InfoSec-Talk-Materials/Intro-To-Linux-Filesystems-SecDSM-May-2019/Xe1phix-Linux-FileSystems-Slides/IntroToLinuxFilesystems-v9.9.pptx)
- [CentralCon - Encrypting Files With Friends Using GnuPG On Parrot Linux](https://gitlab.com/xe1phix/ParrotSecWiki/blob/InfoSecTalk/Xe1phix-InfoSec-Talk-Materials/Encrypting-Files-With-Friends-Using-GnuPG-CentralCon-October-20th-2018/Slides/Xe1phix-Encrypting-Files-With-Friends-Using-GnuPG-v4.3.odp)
- [CornCon 4 - Building A Restricted and Trustworthy Linux Environment](https://gitlab.com/xe1phix/ParrotSecWiki/blob/InfoSecTalk/Xe1phix-InfoSec-Talk-Materials/Building%20A%20Restricted%20&%20Trustworthy%20Environment%20With%20Parrot%20Linux-Corncon2018/Slides/Xe1phix-ParrotSec-InfoSecTalk-2018-v5.4.odp)
- [SecDSM - How To Create A Persistent, LUKS Encrypted USB Device](https://gitlab.com/xe1phix/ParrotSecWiki/blob/InfoSecTalk/Xe1phix-InfoSec-Talk-Materials/How-To-Create-A-Persistent-LUKS-Encrypted-USB-Device-With-Parrot-Linux-SecDSM-October18th-2018/Slides/Xe1phix-ParrotSec-InfoSecTalk-2018-v5.4.odp)
- [CornCon - How To Create A Hackers Swiss Army Knife With Parrot Linux](https://gitlab.com/xe1phix/ParrotSecWiki/tree/InfoSecTalk/Xe1phix-InfoSec-Talk-Materials/How-To-Create-A-Hackers-Swiss-Army-Knife-With-Parrot-Linux-Corncon2017/Slides)


## Xe1phix - Video Tutorials:

<p align="center">
  <a href="https://archive.org/details/@xe1phix">
    <img src="https://img.shields.io/badge/Archive.org-%40Xe1phix-blue?style=flat" alt="Twitter @Xe1phix">
  </a>
  <a href="https://www.bitchute.com/channel/U0QCI90XuSH9/">
    <img src="https://img.shields.io/badge/Bitchute-%40Xe1phix-red?style=flat" alt="Bitchute @Xe1phix">
  </a>
  <a href="https://www.youtube.com/channel/UC4rzx4VToyHJDWbAEJ5cMxQ/videos">
    <img src="https://img.shields.io/badge/Bitchute-%40Xe1phix-red?style=flat" alt="YouTube @Xe1phix">
  </a>
  <a href="https://peertube.video/accounts/xe1phix/videos">
    <img src="https://img.shields.io/badge/PeerTube.Video-%40Xe1phix-red?style=plastic&logo=youtube" alt="@Xe1phix PeerTube.Video">
  </a>
  <a href="https://peertube.live/accounts/xe1phix/videos">
    <img src="https://img.shields.io/badge/PeerTube.Live-%40Xe1phix-red?style=plastic&logo=youtube" alt="@Xe1phix PeerTube.Live">
  </a>
  <a href="https://open.tube/accounts/xe1phix/videos">
    <img src="https://img.shields.io/badge/Open.Tube-%40Xe1phix-red?style=plastic&logo=youtube" alt="@Xe1phix Open.Tube">
  </a>
  <a href="https://peertube.linuxrocks.online/accounts/xe1phix/videos">
    <img src="https://img.shields.io/badge/PeerTube.LinuxRocks-%40Xe1phix-red?style=plastic&logo=youtube" alt="@Xe1phix PeerTube.LinuxRocks">
  </a>
  <a href="https://dlive.tv/Xe1phix">
    <img src="https://img.shields.io/badge/DLive-%40Xe1phix-red?style=plastic&logo=youtube" alt="@Xe1phix DLive">
  </a>                      
</p>

- [Xe1phix - YouTube Tutorials](https://www.youtube.com/channel/UC4rzx4VToyHJDWbAEJ5cMxQ/videos)
- [Xe1phix - Bitchute Tutorials](https://www.bitchute.com/channel/U0QCI90XuSH9/)
- [Xe1phix - Archive.org Tutorials](https://archive.org/details/@xe1phix)


## Xe1phix - Wiki Tutorials:
- [Xe1phix - Gitlab Tutorials](https://gitlab.com/xe1phix/ParrotSecWiki/tree/InfoSecTalk)
- [Xe1phix - Instructables Tutorials](https://www.instructables.com/member/xe1phix/)
- [Xe1phix - LinuxWiki Tutorials](https://gitlab.com/xe1phix/xe1phix-linuxwiki)
- [Xe1phix - ParrotSec Wiki Archived - AnonSurf Tutorial](https://gitlab.com/xe1phix/ParrotLinux-Public-Kiosk-Project/-/blob/master/Xe1phix-%5BDarknet%5D/Xe1phix-Darknet-Wiki/Xe1phix-%5BAnonSurf%5D-ParrotSec-Wiki/Xe1phix-%5BParrotSec-Wiki%5D-Archived-%5BAnonSurf%5D-Tutorial-%5Bv2.2%5D.txt)


# Xe1phix - InfoSec Contact Information:
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|     Application     |     Identifier     |                              Links                              |                                          Fingerprints                                          |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|        Slack        |      @xe1phix      |                     https://secdsm.slack.com                    |                                                                                                |
|       Telegram      |      @Xe1phix      |                       https://t.me/xe1phix                      |                                                                                                |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|  ParrotSec Telegram |      @Xe1phix      |                   https://t.me/parrotsecgroup                   |                                                                                                |
|   ParrotSec Matrix  | @xe1phix:matrix.org|                      #parrotsec:matrix.org                      |                                                                                                |
|    SecDSM Discord   |    @xe1phix#9813   |         https://discord.com/channels/399328329385246730/        |                                                                                                |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|        Matrix       |      @Xe1phix      |                https://matrix.to/#/@xe1phix:matrix.org          |                                                                                                |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|       Discord       |   @xe1phix#9813    |                                                                 |                                                                                                |
|    SecDSM Discord   |      @xe1phix      |         https://discord.com/channels/399328329385246730/        |                                                                                                |
|      VX Discord     |      @xe1phix      |         https://discord.com/channels/1002774831961481247/       |                                                                                                |
|    OpenNIC Discord  |      @xe1phix      |         https://discord.com/channels/464923726883061800/        |                                                                                                |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|       Twitter       |      @Xe1phix      |                   https://twitter.com/xe1phix                   |                                                                                                |
|       LinkedIn      |     Mark Curry     | https://linkedin.com/in/mark-curry-linux-infosec-professional/  |                                                                                                |
|        Gettr       |       @Xe1phix      |                 https://gettr.com/user/xe1phix                  |                                                                                                |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|        Reddit       |      @Xe1phix      |             https://www.reddit.com/user/xe1phix                 |                                                                                                |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|    StackExchange    |      @Xe1phix      |        https://unix.stackexchange.com/users/379197/xe1phix      |                                                                                                |
|    StackOverflow    |      @Xe1phix      |          https://stackoverflow.com/users/12099354/xe1phix       |                                                                                                |
|      AskUbuntu      |      @xe1phix      |             https://askubuntu.com/users/1632297/xe1phix         |                                                                                                |
|   LinuxQuestions    |      @xe1phix      | https://www.linuxquestions.org/questions/user/xe1phix-1261412/  |                                                                                                |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|    Instructables    |      @Xe1phix      |             https://www.instructables.com/member/xe1phix/       |                                                                                                |
|       Write.as      |      @Xe1phix      |                    https://write.as/xe1phix/                    |                                                                                                |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|        Gitlab       |      @Xe1phix      |            https://gitlab.com/users/xe1phix/projects            |                                                                                                |
|       Pastebin      |      @Xe1phix      |                 https://pastebin.com/u/xe1phix                  |                                                                                                |
|         Gist        |      @xe1phix      |                https://gist.github.com/xe1phix                  |                                                                                                |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|      ProtonMail     |      @Xe1phix      |                  markrobertcurry@protonmail.com                 |        https://gitlab.com/xe1phix/Gnupg/blob/master/publickey.xe1phix_protonmail.ch.asc        |
|      ProtonMail     |      @Xe1phix      |                      xe1phix@protonmail.ch                      |        https://gitlab.com/xe1phix/Gnupg/blob/master/publickey.xe1phix_protonmail.ch.asc        |
|      ProtonMail     |  @ParrotSec-Kiosk  |                  ParrotSec-Kiosk@protonmail.ch                  |        https://gitlab.com/xe1phix/Gnupg/blob/master/publickey.xe1phix_protonmail.ch.asc        |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|        GMail        |      @Xe1phix      |                    markrobertcurry@gmail.com                    |                                                                                                |
|        GMail        |      @Xe1phix      |                        xe1phix@gmail.com                        |                                                                                                |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|     Libera.Chat     |      @Xe1phix      |                    https://web.libera.chat/                     |                                                                                                |
|    KillYourTV IRC   |      @Xe1phix      |                        irc.killyourtv.i2p                       |                                                                                                |
|   Echelons I2P IRC  |      @Xe1phix      |                       irc.echelon.i2p:6667                      |                                                                                                |
|       I2P Mail      |      @Xe1phix      |                         xe1phix@mail.i2p                        |                                                                                                |
|     I2PBote Mail    |      @Xe1phix      |                         xe1phix@i2pboe                          |                                                                                                |
| BitMessage I2P Mail |      @Xe1phix      |                      xe1phix@bitmessage.org                     |                                                                                                |
|   Postman I2P IRC   |      @Xe1phix      |                       irc.postman.i2p:6667                      |                                                                                                |
|   Postman SMTP I2P  |      @Xe1phix      |                       smtp.postman.i2p:25                       |                                                                                                |
|  Susimail I2P Mail  |      @Xe1phix      |                 https://127.0.0.1:7657/susimail/                |                                                                                                |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|       Ricochet      |                    |                                                                 |                                                                                                |
|        Jitsi        |      @Xe1phix      |                                                                 |                                                                                                |
|        Signal       |      @Xe1phix      |                                                                 |                                                                                                |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|        id3nt        |      @Xe1phix      |                        https://id3nt.i2p                        |                                                                                                |
|       Mastodon      |      @Xe1phix      |                                                                 |                                                                                                |
|      GNU Social     |      @Xe1phix      |                       https://gnusocial.no                      |                                                                                                |
|       Diaspora      |      @Xe1phix      |                                                                 |                                                                                                |
|         IEEE        |  Member #98670873  |                                                                 |                                                                                                |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|     Archive.org     |      @Xe1phix      |               https://archive.org/details/@xe1phix              |                                                                                                |
|       YouTube       |      @Xe1phix      |   https://youtube.com/channel/UC4rzx4VToyHJDWbAEJ5cMxQ/videos   |                                                                                                |
|       Bitchute      |      @Xe1phix      |          https://www.bitchute.com/channel/U0QCI90XuSH9/         |                                                                                                |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|        Giphy        |      @xe1phix      |                 https://giphy.com/channel/Xe1phix               |                                                                                                |
|      Soundcloud     |      @Xe1phix      |                  https://soundcloud.com/xe1phix                 |                                                                                                |
|      Deviantart     |      @xe1phix      |                https://www.deviantart.com/xe1phix               |                                                                                                |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|    PeerTube.Video   |      @Xe1phix      |          https://peertube.video/accounts/xe1phix/videos         |                                                                                                |
|    PeerTube.Live    |      @Xe1phix      |          https://peertube.live/accounts/xe1phix/videos          |                                                                                                |
| PeerTube.LinuxRocks |      @Xe1phix      |    https://peertube.linuxrocks.online/accounts/xe1phix/videos   |                                                                                                |
|      Open.Tube      |      @Xe1phix      |            https://open.tube/accounts/xe1phix/videos            |                                                                                                |
|        DLive        |      @Xe1phix      |                   https://dlive.tv/Xe1phix                      |                                                                                                |
|       P2PTube       |      @xe1phix      | https://p2ptube.us/c/xe1phix_cipherpunk_linux_tutorial_series   |                                                                                                |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|  Plaintext GPG Key  |     Xe1phix.txt    |     https://gitlab.com/xe1phix/Gnupg/blob/master/Xe1phix.txt    |                            8C2731DD2541089E88181251760286DD6EC3F80D                            |
|  Plaintext GPG Sig  |   Xe1phix.txt.asc  |   https://gitlab.com/xe1phix/Gnupg/blob/master/Xe1phix.txt.asc  |                            8C2731DD2541089E88181251760286DD6EC3F80D                            |
|  Xe1phix GnuPG Key  |     Xe1phix.asc    |     https://gitlab.com/xe1phix/Gnupg/blob/master/Xe1phix.asc    |                            8C2731DD2541089E88181251760286DD6EC3F80D                            |
|   GnuPG Key SHA512  | Xe1phix.asc.sha512 | https://gitlab.com/xe1phix/Gnupg/blob/master/Xe1phix.asc.sha512 |                                                                                                |
|  GnuPG Fingerprints |  Fingerprints.txt  |                 https://gitlab.com/xe1phix/Gnupg                |                            8C2731DD2541089E88181251760286DD6EC3F80D                            |
|       Keybase       |      @Xe1phix      |                    https://keybase.io/xe1phix                   |                            8C2731DD2541089E88181251760286DD6EC3F80D                            |
| Keybase Curl GPG Key|      @Xe1phix      |             https://keybase.io/xe1phix/pgp_keys.asc             |  https://keybase.io/xe1phix/pgp_keys.asc?fingerprint=7a7926f3a537bdf2cc0b66ba8a8a9b4ab2085da5  |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|  Sks-Keyservers.net |      @Xe1phix      |                    https://sks-keyservers.net                   | https://sks-keyservers.net/pks/lookup?op=get&search=0x8C2731DD2541089E88181251760286DD6EC3F80D |
| Sks-Keyserver Onion |      @Xe1phix      |                      jirk5u4osbsr34t5.onion                     |                            8C2731DD2541089E88181251760286DD6EC3F80D                            |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|                     |                    |                                                                 |                                                                                                |
|   ParrotSec Forums  |      @Xe1phix      |           https://community.parrotlinux.org/u/xe1phix           |                                                                                                |
|    Whonix Forums    |      @Xe1phix      |                    https://forums.whonix.org/                   |                                                                                                |
|      I2P Forums     |      @xe1phix      |                        https://forum.i2p                        |                                                                                                |
|    HackBB Forums    |        @???        |                      clsvtzwzdgzkjda7.onion                     |                                                                                                |
|   WildersSecurity   |      @Xe1phix      |       https://www.wilderssecurity.com/members/xe1phix.158561/   |                                                                                                |
|    Gentoo Forums    |      @Xe1phix      |                     http://forums.gentoo.org                    |                                                                                                |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|  Libera.Chat WebIRC |      @xe1phix      |                    https://web.libera.chat/                     |
|   Freenode WebIRC   |      @Xe1phix      |                  https://webchat.freenode.net/                  |                                                                                                |
|     OFTC WebIRC     |      @Xe1phix      |                    https://webchat.oftc.net/                    |                                                                                                |
|    OFTC IRC Onion   |      @Xe1phix      |                      37lnq2veifl4kar7.onion                     |                                                                                                |
|     Rizon WebIRC    |      @Xe1phix      |                    https://www.rizon.net/chat                   |
|    Indymedia IRC    |      @Xe1phix      |                        irc.indymedia.org                        |                                                                                                |
| Indymedia IRC Onion |      @Xe1phix      |                      h7gf2ha3hefoj5ls.onion                     |                                                                                                |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|   Parrotsec WebIRC  |      @Xe1phix      |                https://web.libera.chat/#parrotsec               |                                                                                                |
|   Parrotsec WebIRC  |      @Xe1phix      |             https://webchat.freenode.net/#parrotsec             |                                                                                                |
|    AskNoah WebIRC   |      @Xe1phix      |            https://webchat.freenode.net/#asknoahshow            |                                                                                                |
|   Central Iowa LUG  |      @Xe1phix      |               https://webchat.freenode.net/#cialug              |                                                                                                |
|      I2P WebIRC     |      @Xe1phix      |              https://webchat.oftc.net/?channels=i2p             |                                                                                                |
|      Whonix IRC     |      @Xe1phix      |          https://webirc.oftc.net:8443/?channels=Whonix          |                                                                                                |
|       Tor IRC       |      @Xe1phix      |            https://webirc.oftc.net:8443/?channels=Tor           |                                                                                                |
|:-------------------:|:------------------:|:---------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|


